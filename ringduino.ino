#include "src/imu.h"
#include "src/ring.h"

#define PIXEL_TYPE NEO_GRB + NEO_KHZ800
#define PIXEL_COUNT 16
#define PIXEL_PIN 3

#define LEFT_BUTTON_PIN 2
#define RIGHT_BUTTON_PIN 0
#define BUZZER_PIN 14

IMU imu;
Ring ring(PIXEL_TYPE, PIXEL_COUNT, PIXEL_PIN);

uint16_t led, previous_led, led_to_find = 0;
int now;

uint16_t mapLedPosition(int16_t pos)
{
  if (pos >= PIXEL_COUNT)
    return (pos - PIXEL_COUNT);
  else if (pos < 0)
    return (pos + PIXEL_COUNT);
  else
    return (pos);
}

void setup()
{
  pinMode(LEFT_BUTTON_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  ring.test();
  ring.clear();
  led_to_find = random(15);
  ring.setPixel(led_to_find, 0, 0, 60);

  digitalWrite(BUZZER_PIN, HIGH);
  delay(1000);
  digitalWrite(BUZZER_PIN, LOW);
  ring.clear();
}

void loop()
{
  now = millis();

  imu.readIMU();

  int pos = imu.getAngle() / (360 / PIXEL_COUNT);

  led = mapLedPosition(pos);
  if (led != previous_led)
  {
    ring.clear();
    ring.setPixel(led, 50, 10, 50);
  }
  previous_led = led;

  int but = digitalRead(LEFT_BUTTON_PIN);
  if (but == 0)
  {
    ring.clear();
    if (led == led_to_find)
    {
      for (int i = 0; i < PIXEL_COUNT; i++)
      {
        ring.setPixel(i, 0, 120, 0);
        delay(100);
      }
      delay(2000);
      ring.clear();
    }
    else
    {
      for (int i = 0; i < PIXEL_COUNT; i++)
      {
        ring.setPixel(i, 120, 0, 0);
        delay(100);
      }
      digitalWrite(BUZZER_PIN, HIGH);
      delay(2000);
      ring.clear();
      ring.strob();
      digitalWrite(BUZZER_PIN, LOW);
    }
  }
}
