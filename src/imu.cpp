#include "imu.h"

IMU::IMU()
{
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin();
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
#endif
    imu.initialize();
}

void IMU::readIMU()
{
    imu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    angley = 0.98 * (angley + float(gy) * 0.01 / 131) + 0.02 * atan2((double)ax, (double)az) * 180 / PI;
    anglex = 0.98 * (anglex + float(gx) * 0.01 / 131) + 0.02 * atan2((double)ay, (double)az) * 180 / PI;
    //anglez = 0.98 * (anglez + float(gz) * 0.01 / 131) + 0.02 * atan2((double)ax, (double)ay) * 180 / PI;
    angle = atan((angley / anglex)) * 180 / 3.14;

    if (angle > 0.0)
    {
        if (anglex < 0.0)
            angle += 180;
    }
    else
    {
        if (angley > 0.0)
            angle += 180;
        else
            angle += 360;
    }

    if (angle == 360.0)
        angle = 0;
}

float IMU::getAngleY()
{
    return angley;
}

float IMU::getAngleX()
{
    return anglex;
}

float IMU::getAngle()
{
    return angle;
}