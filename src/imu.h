#ifndef _IMU_H
#define _IMU_H

#include "Arduino.h"
#include "math.h"

#include "I2Cdev.h"
#include "MPU6050.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#define OUTPUT_READABLE_ACCELGYRO

class IMU
{
public:
    IMU();

    float getAngleY();
    float getAngleX();
    float getAngle();
    void readIMU();

private:
    MPU6050 imu;
    int16_t ax, ay, az;
    int16_t gx, gy, gz;
    float anglex;
    float angley;
    float angle;
};

#endif