#include "ring.h"

Ring::Ring(neoPixelType t, uint16_t c, uint16_t p)
{
    nb_pixels = c;
    strip.updateType(t);
    strip.updateLength(c);
    strip.setPin(p);
    strip.begin();
    strip.clear();
    strip.show();
}

void Ring::test()
{
    strip.begin();
    strip.clear();
    for (int i = 0; i < nb_pixels; i++)
    {
        strip.setPixelColor(i, i * 7, i * 7 + 10, 10);
        strip.show();
        delay(500 - i * 30);
    }
    delay(500);
    strip.clear();
}

void Ring::setPixel(uint16_t n, uint8_t r, uint8_t g, uint8_t b)
{
    strip.begin();
    strip.setPixelColor(n, r, g, b);
    strip.show();
}

void Ring::strob()
{
    strip.begin();
    strip.clear();
    strip.show();
    int32_t t = 20;
    while (t > 0)
    {
        for (int i = 0; i < nb_pixels; i++)
        {
            strip.setPixelColor(i, random(100), random(100), random(100));
        }
        strip.show();
        delay(10);
        strip.clear();
        strip.show();
        delay(100);
        t -= 1;
    }
}

void Ring::clear()
{
    strip.begin();
    strip.clear();
    strip.show();
}