#ifndef _RING_H
#define _RING_H

#include "Arduino.h"
#include <Adafruit_NeoPixel.h>

class Ring
{
public:
    Ring(neoPixelType t, uint16_t c, uint16_t p);
    void test();
    void setPixel(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
    void clear();
    void strob();

private:
    uint16_t nb_pixels;
    Adafruit_NeoPixel strip;
};

#endif